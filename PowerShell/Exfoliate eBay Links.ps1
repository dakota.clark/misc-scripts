# Written by Colby Bouma
# Pulls the "dirty" eBay link from your clipboard,
# cleans it, then opens the clean link in your default browser.
# https://gitlab.com/ColbyBouma/misc-scripts/-/blob/master/PowerShell/Exfoliate%20eBay%20Links.ps1

$ListingId = ([Uri](Get-Clipboard)).Segments[-1]
if ( $ListingId ) {

    $CleanLink = "https://www.ebay.com/itm/$ListingId"
    $CleanLink
    Start-Process "$CleanLink"

}