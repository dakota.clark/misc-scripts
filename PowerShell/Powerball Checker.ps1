# Written by Colby Bouma
# Checks a CSV of Powerball ticket lines against the winning numbers.
# The process is currently manual. You have to create a CSV of your tickets by hand,
# give this script the winning numbers with -PowerballNumbers, the multiplier, and the jackpot amount.
# https://gitlab.com/ColbyBouma/misc-scripts/-/blob/master/PowerShell/Powerball%20Checker.ps1

param (
    [Parameter(Mandatory = $true)]
    [String]
    $PowerballLinesFile,

    [Parameter(Mandatory = $true)]
    [Array]
    $PowerballNumbers,

    [Parameter(Mandatory = $true)]
    [ValidateSet(1, 2, 3, 4, 5, 10)]
    [UInt32]
    $PowerPlayMultiplier,

    [UInt64]
    $Jackpot = 999999999
)

if ( -not ( Test-Path $PowerballLinesFile ) ) {

    throw "Unable to find or access $PowerballLinesFile"

}

if ( $PowerballNumbers.Count -ne 6 ) {

    throw "You must provide an array for -PowerBall numbers with all 5 White balls and the Red ball at the end."

}

$PowerballLines = Import-Csv $PowerballLinesFile



$RowCount = 0
$TotalWinnings = 0
ForEach ( $Row in $PowerballLines ) {

    $RowCount ++
    $RowMatchesArray = @()

    # Check the white balls
    For ( $FieldCount = 1; $FieldCount -le 5; $FieldCount ++ ) {

        $FieldValue = $Row.$FieldCount
        if ( $FieldValue -in $PowerballNumbers[0..4] ) {

            $RowMatchesArray += "Y"

        } else {

            $RowMatchesArray += "."

        }

    }

    # Check the red ball
    if ( [Int32]$Row.6 -eq $PowerballNumbers[5] ) {

        $RowMatchesArray += "Y"

    } else {

        $RowMatchesArray += "."

    }

    # https://blogs.technet.microsoft.com/heyscriptingguy/2008/06/02/hey-scripting-guy-how-can-i-use-leading-zeroes-when-displaying-a-value-in-windows-powershell/
    $FormattedRowCount = $RowCount.ToString("00")
    $RowWinnings = 0

    $WhiteMatchesCount = ( $RowMatchesArray[0..4] | Select-String "Y" ).Count
    # Calculate winnings based on the chart available here: https://www.powerball.com/
    switch ( "$WhiteMatchesCount$($RowMatchesArray[5])" ) {

        "0Y" {
            $RowWinnings = 4 * $PowerPlayMultiplier
            break
        }

        "1Y" {
            $RowWinnings = 4 * $PowerPlayMultiplier
            break
        }

        "2Y" {
            $RowWinnings = 7 * $PowerPlayMultiplier
            break
        }

        "3." {
            $RowWinnings = 7 * $PowerPlayMultiplier
            break
        }

        "3Y" {
            $RowWinnings = 100 * $PowerPlayMultiplier
            break
        }

        "4." {
            $RowWinnings = 100 * $PowerPlayMultiplier
            break
        }

        "4Y" {
            $RowWinnings = 50000 * $PowerPlayMultiplier
            break
        }

        "5." {
            if ( $PowerPlayMultiplier -gt 1 ) {

                $RowWinnings = 2000000

            } else {

                $RowWinnings = 1000000

            }
            break
        }

        "5Y" {
            $RowWinnings = $Jackpot
            break
        }

        default {
            break
        }

    }

    $TotalWinnings += $RowWinnings

    '{0} - ${1} - {2}' -f $FormattedRowCount, $RowWinnings, "$RowMatchesArray"

}

'{0}: ${1}' -f "Total Winnings", $TotalWinnings