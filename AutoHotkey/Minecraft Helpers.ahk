; Written by Colby Bouma
; This is a collection of little functions I wrote to prevent carpal tunnel while playing skyblock-style modpacks.
; https://gitlab.com/ColbyBouma/misc-scripts/-/blob/master/AutoHotkey/Minecraft%Helpers.ahk

; CTRL + WIN + 1
; Press SHIFT repeatedly.
; Very useful for growing trees in packs that support twerking.
^#1::
{
	
	Sleep, 200
	
	Loop, 50
	{
		
		SendInput {LShift Down}
		Sleep, 70
		SendInput {LShift Up}
		Sleep, 70
		
	}
	
}
Return

; Hold down right-click.
; Right-click to undo, or use the next function.
^#2::
{
	
	Sleep, 200
	
	SendInput {RButton Down}
	
}
Return 

; Release right-click.
; This is a holdover from when I used this script with Synergy. Not really necessary anymore.
^#3::
{
	
	Sleep, 200
	
	SendInput {RButton Up}
	
}
Return 

; Hold down left-click.
; Left-click to undo, or use the next function.
^#4::
{
	
	Sleep, 200
	
	SendInput {LButton Down}
	
}
Return 

; Release left-click.
^#5::
{
	
	Sleep, 200
	
	SendInput {LButton Up}
	
}
Return 

; Press left-click repeatedly.
^#6::
{
	
	Sleep, 200
	
	Loop, 100
	{
		
		SendInput {Click Left}
		Sleep, 100
		
	}
	
}
Return 

; Hold down left-shift.
^#7::
{
	
	Sleep, 200
	
	SendInput {LShift Down}
	
}
Return