; Written by Colby Bouma
; This script is for pasting data into fields that have disabled pasting, such as the Runescape launcher.
; https://gitlab.com/ColbyBouma/misc-scripts/-/blob/master/AutoHotkey/Fancy%20Paste.ahk

; CTRL + WIN + V
^#v::
{

    ; A small buffer for the human to release the keys.
    Sleep, 500

    ; Delay keypresses by 30 milliseconds and hold each key for 20 milliseconds.
    ; https://autohotkey.com/docs/commands/SetKeyDelay.htm
    SetKeyDelay, 30, 20

    ; Type out the contents of the clipboard.
    ; {Text} keeps it from trying to interpret most characters.
    ; https://autohotkey.com/docs/commands/Send.htm
    Send {Text}%clipboard%

    ; Clear the clipboard.
    ; https://autohotkey.com/docs/misc/Clipboard.htm
    clipboard =
    
}